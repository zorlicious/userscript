// ==UserScript==
// @name        dev-jira diff
// @namespace   test
// @include     https://dev-jira.1and1.org/browse/*
// @version     1
// @grant       GM_addStyle
// @require     https://cemerick.github.io/jsdifflib/difflib.js
// @require     https://cemerick.github.io/jsdifflib/diffview.js
// @require     https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js
// ==/UserScript==

$('div.changehistory table tbody tr').has('td:contains("Description")').each(function() {
  var old_cell = $(this).find('td.activity-old-val');
  var new_cell = $(this).find('td.activity-new-val');
  var diff_cell = $("<td colspan=2></tr>").appendTo(this);
  var output = $("<div class=diffview></div>").appendTo(diff_cell);
  diffUsingJS(old_cell.text().substring(2), new_cell.text().substring(1), output[0]);
  old_cell.remove();
  new_cell.remove();
})

GM_addStyle(
  ".diff th { display: none; }" +
    "table.diff td { font-family: monospace; font-size: 12px; }" +
  "table.diff .delete { background-color: #E99; }" +
  "table.diff .empty { background-color:#DDD; }" +
  "table.diff .replace { background-color:#FD8; }" +
  "table.diff .insert {	background-color:#9E9; }" +
  "table.diff .skip {	background-color:#EFEFEF;	border:1px solid #AAA;	border-right:1px solid #BBC;}");

function diffUsingJS(old_val, new_val, output) {
    var base = difflib.stringAsLines(old_val);
    var newtxt = difflib.stringAsLines(new_val);

    // create a SequenceMatcher instance that diffs the two sets of lines
    var sm = new difflib.SequenceMatcher(base, newtxt);

    // get the opcodes from the SequenceMatcher instance
    // opcodes is a list of 3-tuples describing what changes should be made to the base text
    // in order to yield the new text
    var opcodes = sm.get_opcodes();
    var contextSize = contextSize ? contextSize : null;

    // build the diff view and add it to the current DOM
    output.appendChild(diffview.buildView({
        baseTextLines: base,
        newTextLines: newtxt,
        opcodes: opcodes,
        // set the display titles for each resource
        baseTextName: "Base Text",
        newTextName: "New Text",
        contextSize: contextSize,
        viewType: 1 // inline
    }));
}
