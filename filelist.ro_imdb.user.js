// ==UserScript==
// @name        filelist.ro IMDb
// @namespace   ziggy
// @description Replaces the link on the category icons with the IMDb link
// @include     http://filelist.ro/browse.php*
// @version     1
// @grant       none
// ==/UserScript==

function imdbLink(responseText) {
  var html = new DOMParser().parseFromString(responseText, 'text/html');
  var links = html.querySelectorAll('a');
  for (var i = 0; i < links.length; i++) {
    if (links[i].href.contains('http://www.imdb.com/title')) {
      return links[i].search.substr(1);
    }
  }
}

function markVisited(url) {
  var current_url = window.location.toString();
  history.replaceState({}, "", url);
  history.replaceState({}, "", current_url);
}

function replaceLink(link) {
  link.hidden = true;
  var details = link.parentElement.parentElement.nextSibling.querySelector('a').href
  var request = new XMLHttpRequest();
  request.open('GET', details);
  request.onreadystatechange = function () {
    if (request.readyState === XMLHttpRequest.DONE) {
      var replacement = imdbLink(request.responseText);
      if (replacement) {
        link.href = replacement;
        link.hidden = false;
        link.onmouseup = e => markVisited(details);
      }
    }
  }
  request.send();
}

var icons = document.querySelectorAll('.torrentrow > .torrenttable:first-child a');
for (var i = 0; i < icons.length; i++) {
  replaceLink(icons[i]);
}
