// ==UserScript==
// @name        price.ro
// @namespace   ziggy
// @include     http://www.price.ro/*
// @version     1
// @grant       none
// ==/UserScript==
function prod_referal_url(store_id, prod_id, pret_id) {
  return ('/preturi/referal_product.php?store_id='+store_id+'&prod_id='+prod_id+'&pret_id='+pret_id);
}
function store_url_referal_url(store_id,url,id) {
  return ('/preturi/click_on_store_url.php?store_id='+store_id+'&url='+url+'&id='+id);
}

function eval_referal(index, val) {
  return eval(val.replace('prod_referal', 'prod_referal_url').replace('store_url_referal', 'store_url_referal_url'));
}

$(document).ready(function () {
  $("a[href*='_referal']").attr('href', eval_referal);
});
